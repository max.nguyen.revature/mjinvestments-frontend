# PROJECT NAME

## Project Description

MJ Investments is a web application that allows retail investors to directly invest into startups to venture into green pasture markets and collectively become angel investors for promising young companies. Users can browse stocks on their homepage, view their investment portfolios, and eventually begin investing. The website also features an admin only landing page that allows for the addition of new stocks and the viewing of customer information.

## Technologies Used

* HTML
* CSS
* React Bootstrap
* React Typescript
* Spring Framework
* Spring Data
* Spring Boot


## Features

* User login and registration
* Stock browsing
* Stock purchasing
* User portfolio
* Admin landing page
* Admin addition of new stocks
* Admin view customer information
* User profile

To-do list:
* More stock metrics
* Addition of an external API

## Getting Started
For the backend:
`git clone https://gitlab.com/max.nguyen.revature/mjinvestments-backend`
Run the spring application main file.

For the frontend:
`git clone https://gitlab.com/max.nguyen.revature/mjinvestments-frontend`
`cd mjinvestments-frontend`
`npm install`
`npm start`


## Contributors

Jason Lam, Jason Oh, Max Nguyen

