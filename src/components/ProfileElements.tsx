import { NavLink as Link} from 'react-router-dom';
import styled from 'styled-components';

export const Container =styled.nav`
background-color: grey;
display: grid;
grid-template-rows: max-content 50px 1fr;
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
border-radius: 10px;
width 40rem;
padding: 60px;
margin-left: auto;
margin-right: auto;
`;