import React, {useState} from 'react';
import Navb from '../components/navbar';
import Profile from '../components/Profile';

export default function ProfilePage() 
{

    return (
        <div >
            <Navb />
            <Profile />

        </div>
    )
}